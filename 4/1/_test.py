from route_between_nodes import route_between_nodes as method
from adjacency_list import Node

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

if __name__ == "__main__":
    s_a = Node("s")
    d_a = s_a

    d_b = Node("Child B")
    s_b = Node("origin", [
        Node("Child A"),
        d_b,
        Node("Child C")
    ])
    
    d_c = Node("Child C", [
        Node("origin")
    ])
    s_c = Node("origin", [
        Node("Child A"),
        Node("Child B")
    ])
    
    d_d = Node("Child D_A_B", [
        Node("origin")
    ])
    s_d = Node("origin", [
        Node("Child A"),
        Node("Child B", [
            Node("Child B_A")
        ]),
        Node("Child C"),
        Node("Child D", [
            Node("Child D_A", [
                Node("Child D_A_A"),
                d_d
            ])
        ]),
    ])
    
    test_cases = [
        ((s_a, d_a), True),
        ((s_b, d_b), True),
        ((s_c, d_c), False),
        ((s_d, d_d), True)
    ]
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
