from typing import List

class Graph:
    def __init__(self, nodes: List["Node"] = []):
        self.nodes = nodes
        
class Node:
    def __init__(self, name: str = "", children: List["Node"] = []):
        self.name = name
        self.children = children
