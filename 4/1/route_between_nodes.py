from typing import List, Set
from adjacency_list import *

def route_between_nodes(s: Node, d: Node, visited: Set[Node] = set()) -> bool:
    # Perform DFS to find a path between s and d if it exists
    # Ensure visited nodes are marked in case of cycles
    if s is d:
        return True
    for child in s.children:
        if child not in visited:
            visited.add(child)
            if route_between_nodes(child, d, visited):
                return True
    return False
