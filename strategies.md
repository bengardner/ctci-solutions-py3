# Strategies

This is a list of problem solving strategies sorted by chapter.

## Arrays and Strings

* Understand StringBuilder, HashTable and ArrayList

## Linked Lists

* The Runner
* Recursion
* Silly value-pointer tricks