class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self) -> str:
        output = "[" + str(self.data) + "]"
        node = self
        while node.next:
            node = node.next
            output += " -> " + "[" + str(node.data) + "]"
        return output

    def __eq__(self, other) -> bool:
        this = self
        that = other
        if this is None or that is None:
            return this is that
        while this is not None and that is not None and this.data == that.data:
            this = this.next
            that = that.next
        return this is that

def remove_dups(head: ListNode) -> ListNode:
    if head is None: return head
    seen = set([head.data])
    prev = head
    cur = head.next
    while cur is not None:
        if cur.data in seen:
            prev.next = cur.next
        else:
            seen.add(cur.data)
            prev = prev.next
        cur = cur.next
    return head

def remove_dups_without_space(head: ListNode) -> ListNode:
    seen = head
    while seen is not None:
        prev = seen
        cur = seen.next
        while cur is not None:
            if cur.data == seen.data:
                prev.next = cur.next
            else:
                prev = prev.next
            cur = cur.next
        seen = seen.next
    return head
