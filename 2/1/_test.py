from remove_dups import remove_dups_without_space as method
from remove_dups import ListNode

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

if __name__ == "__main__":
    test_cases = []

    a = ListNode(1)
    b = ListNode(1)
    test_cases.append([(a,), b])
    
    a = ListNode(1)
    a.next = ListNode(1)
    b = ListNode(1)
    test_cases.append([(a,), b])
    
    a = ListNode(1)
    a.next = ListNode(2)
    b = ListNode(1)
    b.next = ListNode(2)
    test_cases.append([(a,), b])
    
    a = ListNode(1)
    a.next = ListNode(2)
    a.next.next = ListNode(2)
    a.next.next.next = ListNode(2)
    a.next.next.next.next = ListNode(2)
    a.next.next.next.next.next = ListNode(3)
    a.next.next.next.next.next.next = ListNode(1)
    a.next.next.next.next.next.next.next = ListNode(3)
    b = ListNode(1)
    b.next = ListNode(2)
    b.next.next = ListNode(3)
    test_cases.append([(a,), b])
    
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
