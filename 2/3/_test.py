from typing import List

from delete_middle_node import delete_middle_node as method
from delete_middle_node import ListNode

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

def make_linked_list(nodes: List[ListNode]) -> ListNode:
    dummy = cur = ListNode(None)
    for node in nodes:
        cur.next = node
        cur = node
    head = dummy.next
    return head

if __name__ == "__main__":
    # Fill this test case list with tuples of (arguments, expected result)
    test_cases = [
        # ((arg1_1, arg1_2,), result1),
        # ((arg2_1, arg2_2,), result2),
    ]

    middles = [
        ListNode(99),
        ListNode(99)
    ]

    test_lists = [
        [
            ListNode(1),
            middles[0],
            ListNode(3),
        ],
        [
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
            middles[1],
            ListNode(99),
        ],
    ]

    expected_lists = [
        [
            ListNode(1),
            ListNode(3),
        ],
        [
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
            ListNode(99),
        ],
    ]

    for middle, test, expected in zip(middles, test_lists, expected_lists):
        head = (make_linked_list(test))
        exp = make_linked_list(expected)
        test_cases.append(((middle, head), exp))
    
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
