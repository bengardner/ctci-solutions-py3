# Delete Middle Node

Without access to the node previous to the "middle" node, the trick is to make the "middle" node look like its next node. The middle can take the next node's .next and .data to replace its own.