class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self) -> str:
        output = "[" + str(self.data) + "]"
        node = self
        while node.next:
            node = node.next
            output += " -> " + "[" + str(node.data) + "]"
        return output

    def __eq__(self, other) -> bool:
        this = self
        that = other
        if this is None or that is None:
            return this is that
        while this is not None and that is not None and this.data == that.data:
            this = this.next
            that = that.next
        return this is that

def sum_lists(a: ListNode, b: ListNode) -> ListNode:
    a_cur = a
    b_cur = b
    new_list = prev = cur = ListNode(0)
    carry = False
    while a_cur is not None or b_cur is not None:
        if a_cur is not None:
            cur.data += a_cur.data
            a_cur = a_cur.next
        if b_cur is not None:
            cur.data += b_cur.data
            b_cur = b_cur.next
        if carry:
            cur.data += 1
            carry = False
        if cur.data > 9:
            cur.data -= 10
            carry = True
        prev = cur
        cur.next = ListNode(0)
        cur = cur.next
    if carry:
        cur.data = 1
    else:
        prev.next = None    # keep the list clean by removing trailing zeroes
    return new_list
