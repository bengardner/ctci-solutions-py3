# Sum Lists

In this problem we need to iterate over the lists simultaneously, putting the resulting value into a new list. If the result overflows—that is, it exceeds 9—we carry the one into the next value.