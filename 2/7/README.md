# Intersection

The intersection is easy enough to detect: we find identical nodes (by reference) on both linked lists. We can go about finding this by iterating over an entire list n times, comparing each node with a node from the other list. A more efficient solution is to iterate over a list once, capturing each node reference in a hash set. That way, we can iterate over the other list once, checking each node for a match in the hash set to find the intersection.

## Space complexity improvement

There is an improvement over this method in terms of space. Note that intersecting lists have identical tails at the intersection. Therefore, two intersecting lists differ only up until the intersection.

We can iterate over both lists, counting the elements. Take the larger list count and subtract the smaller to find the earliest possible index for an intersection in the larger list. This is the case, because the lists are guaranteed to have equal element counts at the tail of the intersection.

We use the node at this index as a starting point to iteratively compare nodes from each list until we find the intersection.
