from typing import List

from intersection import intersection as method
from intersection import ListNode

class TestCase():
    run_count = 0

    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

def make_linked_list(nodes: List[ListNode]) -> ListNode:
    dummy = cur = ListNode(None)
    for node in nodes:
        cur.next = node
        cur = node
    head = dummy.next
    return head

if __name__ == "__main__":
    # Fill this test case list with tuples of (arguments, expected result)
    test_cases = [
        # ((arg1_1, arg1_2,), result1),
        # ((arg2_1, arg2_2,), result2),
    ]

    test_data = []
    a = "a"
    b = "b"
    exp = "exp"

    x = ListNode('intersection')
    x.next = ListNode('x')
    x.next.next = ListNode('y')
    x.next.next.next = ListNode('z')
    test_data.append({
        a: [
            x,
        ],
        b: [
            x,
        ],
        exp: x
    })
    test_data.append({
        a: [
            ListNode('a'),
            x,
        ],
        b: [
            ListNode('a'),
            x,
        ],
        exp: x
    })
    test_data.append({
        a: [
            ListNode('a'),
            ListNode('b'),
            x,
        ],
        b: [
            x,
        ],
        exp: x
    })
    test_data.append({
        a: [
            ListNode('a'),
            ListNode('b'),
            ListNode('c'),
        ],
        b: [
            x,
        ],
        exp: None
    })

    for data in test_data:
        test_case_args = []
        test_case_args.append(make_linked_list(data[a]))
        test_case_args.append(make_linked_list(data[b]))
        expected_result = data[exp]
        test_cases.append((test_case_args, expected_result))

    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
