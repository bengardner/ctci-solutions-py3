class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self) -> str:
        output = "[" + str(self.data) + "]"
        node = self
        while node.next:
            node = node.next
            output += " -> " + "[" + str(node.data) + "]"
        return output

    def __eq__(self, other) -> bool:
        this = self
        that = other
        if this is None or that is None:
            return this is that
        while this is not None and that is not None and this.data == that.data:
            this = this.next
            that = that.next
        return this is that

def intersection(a: ListNode, b: ListNode) -> ListNode:
    cur_a = a
    cur_b = b
    while cur_a is not None and cur_b is not None:
        cur_a = cur_a.next
        cur_b = cur_b.next
    while cur_a is not None:
        a = a.next
        cur_a = cur_a.next
    while cur_b is not None:
        b = b.next
        cur_b = cur_b.next
    while a is not None and b is not None:
        if a is b:
            return a
        a = a.next
        b = b.next
    return None
