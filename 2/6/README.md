# Palindrome

A palindrome is the same from the head going forward as from the tail going backward. We can traverse the list once to get its contents in order, storing them at the head of a new linked list. Then we can traverse the stored contents, now in the reverse order, and compare them to the original list to see if they are equal.