class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self) -> str:
        output = "[" + str(self.data) + "]"
        node = self
        while node.next:
            node = node.next
            output += " -> " + "[" + str(node.data) + "]"
        return output

    def __eq__(self, other) -> bool:
        this = self
        that = other
        if this is None or that is None:
            return this is that
        while this is not None and that is not None and this.data == that.data:
            this = this.next
            that = that.next
        return this is that

def palindrome(head: ListNode) -> bool:
    comparison_list = ListNode(head.data)
    cur = head
    while cur.next:
        comparison_list, comparison_list.next = ListNode(cur.next.data), comparison_list
        cur = cur.next
    cur = head
    while comparison_list is not None and cur is not None and comparison_list.data == cur.data:
        comparison_list = comparison_list.next
        cur = cur.next
    return comparison_list is None and cur is None
