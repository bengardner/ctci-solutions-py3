# Partition

We can solve this by iterating through the list and moving each node whose value is less than the partition value to the front. We keep two pointers so that we can set the previous node's .next to be .next.next and this node's .next to be the head.