class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self) -> str:
        output = "[" + str(self.data) + "]"
        node = self
        while node.next:
            node = node.next
            output += " -> " + "[" + str(node.data) + "]"
        return output

    def __eq__(self, other) -> bool:
        this = self
        that = other
        if this is None or that is None:
            return this is that
        while this is not None and that is not None and this.data == that.data:
            this = this.next
            that = that.next
        return this is that

def partition(head: ListNode, x: ListNode) -> ListNode:
    cur = head
    while cur.next is not None:
        if cur.next.data < x.data:
            head, cur.next.next, cur.next = cur.next, head, cur.next.next
        else:
            cur = cur.next
    return head
