from typing import List

from partition import partition as method
from partition import ListNode

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

def make_linked_list(nodes: List[ListNode]) -> ListNode:
    dummy = cur = ListNode(None)
    for node in nodes:
        cur.next = node
        cur = node
    head = dummy.next
    return head

if __name__ == "__main__":
    # Fill this test case list with tuples of (arguments, expected result)
    test_cases = [
        # ((arg1_1, arg1_2,), result1),
        # ((arg2_1, arg2_2,), result2),
    ]

    partitions = [
        ListNode(2),
        ListNode(2),
        ListNode(3),
    ]

    test_lists = [
        [
            ListNode(1),
            ListNode(2),
            ListNode(3),
        ],
        [
            ListNode(3),
            ListNode(2),
            ListNode(1),
        ],
        [
            ListNode(3),
            ListNode(2),
            ListNode(1),
            ListNode(2),
            ListNode(2),
            ListNode(4),
            ListNode(3),
            ListNode(1),
        ],
    ]

    expected_lists = [
        [
            ListNode(1),
            ListNode(2),
            ListNode(3),
        ],
        [
            ListNode(1),
            ListNode(3),
            ListNode(2),
        ],
        [
            ListNode(1),
            ListNode(2),
            ListNode(2),
            ListNode(1),
            ListNode(2),
            ListNode(3),
            ListNode(4),
            ListNode(3),
        ],
    ]

    for partition, test, expected in zip(partitions, test_lists, expected_lists):
        head = (make_linked_list(test))
        exp = make_linked_list(expected)
        test_cases.append(((head, partition), exp))
    
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
