from return_kth_to_last import return_kth_to_last as method
from return_kth_to_last import ListNode

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

if __name__ == "__main__":
    # Fill this test case list with tuples of (arguments, expected result)
    test_cases = [
        # ((arg1_1, arg1_2,), result1),
        # ((arg2_1, arg2_2,), result2),
    ]

    nodes_list = [
        [
            ListNode(1),
            ListNode(2),
            ListNode(3),
        ],
        [
            ListNode(1),
            ListNode(2),
            ListNode(3),
        ],
        [
            ListNode(1),
            ListNode(2),
            ListNode(3),
        ],
        [
            ListNode(1),
        ],
    ]

    k_list = [
        1,
        2,
        0,
        0,
    ]

    for i, nodes in enumerate(nodes_list):
        cur = head = ListNode(None)
        for node in nodes:
            cur.next = node
            cur = node
        test_cases.append(((head.next, k_list[i]), nodes[len(nodes)-k_list[i]-1]))
    
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
