# Return Kth to Last

We can solve this in a straightforward manner, counting the number of elements by running through the list once, then running through again count - k times.

We can improve on this by running through once, but keeping an additional pointer k elements behind.