class ListNode():
    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self) -> str:
        output = "[" + str(self.data) + "]"
        node = self
        while node.next:
            node = node.next
            output += " -> " + "[" + str(node.data) + "]"
        return output

    def __eq__(self, other) -> bool:
        this = self
        that = other
        if this is None or that is None:
            return this is that
        while this is not None and that is not None and this.data == that.data:
            this = this.next
            that = that.next
        return this is that

def return_kth_to_last(head: ListNode, k: int) -> ListNode:
    cur = head
    for i in range(k):
        cur = cur.next
    kth = head
    while cur.next is not None:
        cur = cur.next
        kth = kth.next
    return kth
