def is_unique(s: str) -> bool:
    seen = set()
    for c in s:
        if c in seen:
            return False
        seen.add(c)
    return True

"""Without an auxiliary data structure,
you can do it in O(n^2) by checking each letter against every other letter."""
def is_unique2(s: str) -> bool:
    for i, a in enumerate(s):
        for _, b in enumerate(s[i+1:]):
            if a == b:
                return False
    return True

"""If you sort, you can do it in O(nlogn) by checking for adjacent duplicates."""
def is_unique3(s: str) -> bool:
    last = None
    for c in sorted(s):
        if c == last:
            return False
        last = c
    return True

"""Using a bit vector, one bit per letter:"""
def is_unique4(s: str) -> bool:
    seen = 0
    #c0 = 'A'
    for c in s:
        cur = 1 << ord(c)# - ord(c0)
        if cur & seen:
            return False
        seen |= cur
    return True
