def one_away(a: str, b: str) -> bool:
    edited = False
    i = 0
    j = 0
    while i < len(a) and j < len(b):
        if a[i] != b[j]:
            if edited:
                return False
            else:
                if len(a) == len(b) + 1 and a[i + 1] == b[j]:
                    i += 1
                elif len(b) == len(a) + 1 and a[i] == b[j + 1]:
                    j += 1
                elif len(a) == len(b) and a[i + 1] == b[j + 1]:
                    i += 1
                    j += 1
                else:
                    return False
                edited = True
        i += 1
        j += 1
        
    return i == len(a) and j == len(b) or not edited and (i == len(a) - 1 or j == len(b) - 1)
