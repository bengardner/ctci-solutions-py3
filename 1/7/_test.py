from rotate_matrix import rotate_matrix as method

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

if __name__ == "__main__":
    a = [[1]]
    a_r = [[1]]
    b = [
        [1,2,3],
        [4,5,6],
        [7,8,9],
        ]
    b_r = [
        [7,4,1],
        [8,5,2],
        [9,6,3]
        ]
    c = [
        [1,2,3,4],
        [5,6,7,8],
        [13,14,15,17],
        [9,10,11,12]
        ]
    c_r = [
        [9,13,5,1],
        [10,14,6,2],
        [11,15,7,3],
        [12,17,8,4]
        ]
    
    test_cases = [
        ((a,), a_r),
        ((b,), b_r),
        ((c,), c_r),
    ]
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
