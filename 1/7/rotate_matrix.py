from typing import List

def rotate_matrix(m: List[List[int]]) -> List[List[int]]:
    for i in range(len(m) // 2):    # layer
        for j in range(i, len(m) - 1 - i): # column
            temp, m[j][len(m)-1-i] = m[j][len(m)-1-i], m[i][j]
            temp, m[len(m)-1-i][len(m)-1-j] = m[len(m)-1-i][len(m)-1-j], temp
            temp, m[len(m)-1-j][i] = m[len(m)-1-j][i], temp
            m[i][j] = temp
    return m
