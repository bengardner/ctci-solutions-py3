import collections

def check_permutation(a: str, b: str) -> bool:
    letters = collections.defaultdict(int)
    for c in a:
        letters[c] += 1
    for c in b:
        letters[c] -= 1
        if letters[c] == 0:
            del letters[c]
        elif letters[c] < 0:
            return False
    return not letters
