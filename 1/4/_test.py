from palindrome_permutation import palindrome_permutation as method

def test(args, expected) -> bool:
    return method(*args) == expected

def print_result(args, expected):
    print("Test succeeded!" if test(args, expected) else
"""Test failed.
Your output:
\t{res}
Expected:
\t{exp}
""".format(
    res = method(*args),
    exp = expected
    ))

if __name__ == "__main__":
    args = "Tact Coa",
    expected = True
    print_result(args, expected)
