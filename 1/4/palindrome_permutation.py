import collections
def palindrome_permutation(s: str) -> bool:
    # A string can be a palindrome if it has all even counts of letters save one
    counts = collections.defaultdict(int)
    odd = 0
    for c in s.lower():
        if c == " ": continue
        counts[c] += 1
        if counts[c] % 2 == 1:
            odd += 1
        else:
            odd -= 1
    if odd < 2:
        return True
    return False
