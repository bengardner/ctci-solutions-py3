from typing import List

def zero_matrix(m: List[List[int]]) -> List[List[int]]:
    rows = set()
    cols = set()
    for i in range(len(m)):
        for j, elem in enumerate(m[i]):
            if elem == 0:
                rows.add(i)
                cols.add(j)
    for i in range(len(m)):
        for j in range(len(m[i])):
            if i in rows or j in cols:
                m[i][j] = 0
    return m
