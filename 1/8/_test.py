# Uncomment this line and replace "your_module" with the name of your module to test
from zero_matrix import zero_matrix as method

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

if __name__ == "__main__":
    arg1 = [[]]
    result1 = [[]]

    arg2 = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]
    result2 = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ]
    
    arg3 = [
        [1, 0, 3, 7],
        [1, 2, 3, 7],
        [4, 5, 6, 0],
    ]
    result3 = [
        [0, 0, 0, 0],
        [1, 0, 3, 0],
        [0, 0, 0, 0],
    ]

    arg4 = [
        [0, 1, 2, 3],
        [4, 0, 5, 6],
        [7, 8, 0, 9],
        [1, 1, 1, 0],
    ]
    result4 = [
        [0, 0, 0, 0, ],
        [0, 0, 0, 0, ],
        [0, 0, 0, 0, ],
        [0, 0, 0, 0, ],
    ]
    
    # Fill this test case list with tuples of (arguments, expected result)
    test_cases = [
        ((arg1,), result1),
        ((arg2,), result2),
        ((arg3,), result3),
        ((arg4,), result4),
    ]
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
