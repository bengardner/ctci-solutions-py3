def string_compression(s: str) -> str:
    previous = s[0]
    count = 0
    res = ""
    for c in s:
        if c is not previous:
            res += previous + str(count)
            count = 0
        count += 1
        previous = c
    res += previous + str(count)
    return res if len(res) < len(s) else s
