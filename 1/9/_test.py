# Uncomment this line and replace "your_module" with the name of your module to test
from string_rotation import string_rotation as method

class TestCase():
    run_count = 0
    
    def __init__(self, args, expected):
        self.args = args
        self.expected = expected

        self.separator = "=" * 20

    def test(self) -> bool:
        TestCase.run_count += 1
        return method(*self.args) == self.expected

    def print_result(self):
        if self.test():
            print(
"""({count}) ✔ Test passed!
    {args}
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    exp = self.expected,
    separator = self.separator
))
        else:
            print(
"""({count}) ✖ Test failed.
    {args}
    Your output:
        {res}
    Expected:
        {exp}
{separator}
""".format(
    count = TestCase.run_count,
    args = self.args,
    res = method(*self.args),
    exp = self.expected,
    separator = self.separator
))

if __name__ == "__main__":
    args1 = "waterbottle", "erbottlewat"
    result1 = True

    args2 = "", ""
    result2 = True
    
    args3 = "water", "aterww"
    result3 = False

    args4 = "anagram", "nagaram"
    result4 = False
    
    # Fill this test case list with tuples of (arguments, expected result)
    test_cases = [
        (args1, result1),
        (args2, result2),
        (args3, result3),
        (args4, result4),
    ]
    for test_case in test_cases:
        TestCase(test_case[0], test_case[1]).print_result()
