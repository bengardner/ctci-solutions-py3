def string_rotation(s: str, r: str) -> bool:
    return len(s) == len(r) and s in r * 2
