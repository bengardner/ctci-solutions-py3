# String Rotation

For this question, we need to find which factors qualify one string as being a rotation of another.

We know that one such factor is whether it is a substring. However, we cannot use the entire string, but we can use part of it. For any given rotation, a substring at least the size of half the string will be a substring of the original string. That is a necessary but not sufficient condition.

We don't know where this substring lies in the rotated string, only that it exists.

We also know that the rotation must be of equal length and have the same character count as the unrotated string.

How can we use one call to isSubstring to find out whether the given string is a rotation of the original?

This one requires some thinking outside of the box. See hint #104.