def urlify(s: str, n: int) -> str:
    s = [c for c in s]
    res = []
    for i, c in enumerate(s):
        if i == n: break
        if c == ' ':
            res.append('%')
            res.append('2')
            res.append('0')
        else:
            res.append(c)
    return ''.join(res)
