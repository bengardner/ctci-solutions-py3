from typing import Set, Tuple, Dict

"""Start from the bottom-right of the grid (exit) and work to the top-left
(start). At each iteration, move left and up.
In doing so, return the results of the recursive calls with "R" or "D" appended,
OR'd.
If the current cell is bad, return None instead.
Keep a results cache as you go."""
def robot_in_a_grid(r: int, c: int, bad_cells: Set[Tuple[int, int]]) -> str:
    method = find_route_top_down    # Change methods to compare runtimes
    
    cache = {(0, 0): None if (0, 0) in bad_cells else ""}
    i = (r - 1, c - 1)
    return method(r, c, bad_cells, i, cache)

def find_route_top_down(r: int, c: int, x: Set[Tuple[int, int]], i: Tuple[int, int], cache: Dict[Tuple[int, int], str]) -> str:
    if i in cache: return cache[i]
    if i in x or i[0] < 0 or i[1] < 0 or i[0] > r - 1 or i[1] > c - 1: return None
    up = find_route_top_down(r, c, x, (i[0] - 1, i[1]), cache)
    left = find_route_top_down(r, c, x, (i[0], i[1] - 1), cache)
    res = up if up is not None else left
    if up is not None: res += "D"
    elif left is not None: res += "R"
    cache[i] = res
    return res

def find_route_recursive(r: int, c: int, x: Set[Tuple[int, int]], i: Tuple[int, int], cache: Dict[Tuple[int, int], str]) -> str:
    if i == (0, 0): return None if i in x else ""
    if i in x or i[0] < 0 or i[1] < 0 or i[0] > r - 1 or i[1] > c - 1: return None
    up = find_route_recursive(r, c, x, (i[0] - 1, i[1]), cache)
    left = find_route_recursive(r, c, x, (i[0], i[1] - 1), cache)
    res = up if up is not None else left
    if up is not None: res += "D"
    elif left is not None: res += "R"
    cache[i] = res
    return res
