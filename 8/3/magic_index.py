from typing import List

def magic_index(n: List[int]) -> int:
    low = 0
    high = len(n)
    while low <= high:
        mid = low + (high - low) // 2
        if n[mid] < mid:
            low = mid + 1
        elif n[mid] > mid:
            high = mid - 1
        else:
            return n[i]
            
    return -1
