from typing import List

def triple_step(n: int) -> int:
    cache = [1, 2, 4]
    if n < 4:
        return cache[n]
    return meat(n - 3, cache) + meat(n - 2, cache) + meat(n - 1, cache)

def meat(n: int, cache: List[int]) -> int:
    if len(cache) < n:
        cache.append(meat(n - 3, cache) + meat(n - 2, cache) + meat(n - 1, cache))
    return cache[n - 1]
